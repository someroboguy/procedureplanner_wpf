﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace CapstoneDAL
{

    public class DataBaseHelper
    {

        public static string connectionString = "Data Source=localhost\\SQLEXPRESS01;Initial Catalog=Capstone;Integrated Security=SSPI";

        public static DataTable ExecuteSelectQuery(string cmd)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd, sqlConn))
                    {
                        DataTable datatable = new DataTable();
                        da.Fill(datatable);
                        return datatable;
                    }
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        public static void ExecuteSQLNonQuery(string cmd, string param1, object param2)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(cmd, sqlConn))
                    {
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@PatientId", param1);
                        sqlCmd.Parameters.AddWithValue("@ProcedureName", param2);
                        sqlCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        public static object ExecuteSQLScalar(string cmd)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(cmd, sqlConn))
                    {
                        return sqlCmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }
    }
}
