﻿using CapstoneUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Prism.Events;
using CapstoneUI.Model;

namespace CapstoneUI
{
    public class ApplicationViewModel : ObservableObject
    {
        #region Fields
        private LoginViewModel _loginViewModel = new LoginViewModel();
        private PatientSelectionViewModel _patientViewModel = new PatientSelectionViewModel();
        private ICommand _changePageCommand;
        private IPageViewModel _currentPageViewModel;
        private List<IPageViewModel> _pageViewModels;
        public static EventAggregator _eventAggregator;
        #endregion

        public ApplicationViewModel(EventAggregator eventAggregator)
        {
            // Add available pages
            PageViewModels.Add(new LoginViewModel());
            PageViewModels.Add(new PatientSelectionViewModel());
            PageViewModels.Add(new OperationBuilderViewModel());

            // Set starting page
            CurrentPageViewModel = PageViewModels[0];
            _eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<PubSubEvent<IPageViewModel>>().Subscribe((eventArgs) => ChangeViewModel(eventArgs));
        }

        private void NavToOrder(string obj)
        {
            _patientViewModel.UserId = obj;
            CurrentPageViewModel = PageViewModels[1];
        }

        #region Properties / Commands

        public ICommand ChangePageCommand
        {
            get
            {
                if (_changePageCommand == null)
                {
                    _changePageCommand = new RelayCommand(
                        p => ChangeViewModel((IPageViewModel)p),
                        p => p is IPageViewModel);
                }

                return _changePageCommand;
            }
        }

        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (_pageViewModels == null)
                    _pageViewModels = new List<IPageViewModel>();

                return _pageViewModels;
            }
        }

        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                return _currentPageViewModel;
            }
            set
            {
                if (_currentPageViewModel != value)
                {
                    _currentPageViewModel = value;
                    OnPropertyChanged("CurrentPageViewModel");
                }
            }
        }
        #endregion

        #region Methods

        private void ChangeViewModel(IPageViewModel viewModel)
        {
            if (!PageViewModels.Contains(viewModel))
                PageViewModels.Add(viewModel);

            CurrentPageViewModel = PageViewModels
                .FirstOrDefault(vm => vm == viewModel);
        }

        #endregion
    }
}
