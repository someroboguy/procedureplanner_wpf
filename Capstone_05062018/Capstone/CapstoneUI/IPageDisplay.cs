﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneUI
{
    public interface IPageDisplay
    {
        IPageViewModel CurrentPageViewModel();

        void ChangeViewModel(IPageViewModel newPage);
    }
}
