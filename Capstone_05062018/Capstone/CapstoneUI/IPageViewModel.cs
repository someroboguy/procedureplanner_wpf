﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapstoneUI
{
    public interface IPageViewModel
    {
        string Name { get; }
    }
}
