﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneUI.Model
{
    public class LoginModel : ObservableObject
    {
        #region Fields
        private string _Id;
        private string _userName;
        #endregion // Fields

        #region Properties
        public string Id
        {
            get => _Id;
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
        }
        public string UserName
        {
            get => _userName;
            set
            {
                _userName = value;
                OnPropertyChanged("UserName");
            }
        }
        #endregion // Properties
    }
}
