﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneUI.Model
{
    public class OperationBuilderModel : ObservableObject
    {
        public string Id { get; set; }
        public string PatientName { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string BloodType { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Sex { get; set; }
        public string Insurance { get; set; }
        public string Medication { get; set; }
        public string Instructions { get; set; }
        public string Staus { get; set; }
        public string Refills { get; set; }
        public string LastFilledOn { get; set; }
        public string InitiallyOrdered { get; set; }
        public string Quanity { get; set; }
        public string DaysSupply { get; set; }
        public string Pharmacy { get; set; }
        public string PrescriptionNumber { get; set; }
        public string AllergyName { get; set; }
        public string DateEntered { get; set; }
        public string Reaction { get; set; }
        public string AllergyType { get; set; }
        public string DrugClass { get; set; }
        public string ObservedHistorical { get; set; }
        public string ProcedureName { get; set; }
        public string ProcedureCategoryName { get; set; }
        public string ImageSource { get; set; }
        public int OperationProcId { get; set; }
        public string PatientId { get; internal set; }
    }
}
