﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneUI.Model
{
    public class PatientSelectionModel : ObservableObject
    {
        #region Fields
        public string _id;
        public string _patientName;
        #endregion

        #region Properties
        public string Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged("Id");
            }
        }
        public string PatientName
        {
            get => _patientName;
            set
            {
                _patientName = value;
                OnPropertyChanged("PatientName");
            }
        }
        

        #endregion // Properties
    }
}
