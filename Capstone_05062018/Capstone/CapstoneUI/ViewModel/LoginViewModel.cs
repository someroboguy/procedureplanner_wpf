﻿using CapstoneDAL;
using CapstoneUI.HelperClasses;
using CapstoneUI.Model;
using System;
using System.Collections.ObjectModel;
using System.Data;
using Prism.Events;
using System.Windows;
using System.Windows.Input;

namespace CapstoneUI.ViewModel
{
    public class LoginViewModel : IPageViewModel
    {
        #region Fields
        public LoginModel _selectedId;
        public static LoginModel selectedUserId;
        public string Name => "Login";
        public static Func<string> PasswordHandler { get; internal set; }
        private ICommand _userValidationCommand;
        private ObservableCollection<LoginModel> _users;
        public event Action<string> GotoPatientView = delegate { };
        #endregion

        #region Properties/Commands
        public LoginModel SelectedId
        {
            get => _selectedId;
            set
            {
                _selectedId = value;
                selectedUserId = _selectedId;
            }
        }

        public ObservableCollection<LoginModel> Users
        {
            get
            {
                if (this._users == null)
                {
                    this._users = this.GetUsers();
                }
                return this._users;
            }
        }

        public ObservableCollection<LoginModel> GetUsers()
        {
            try
            ]{
                DataTable dtUsers = DataBaseHelper.ExecuteSelectQuery("Select Id, UserName from Users");
                ObservableCollection<LoginModel> usersList = new ObservableCollection<LoginModel>();
                foreach (DataRow dr in dtUsers.Rows)
                {
                    var obj = new LoginModel()
                    {
                        Id = dr["Id"] == DBNull.Value ? "" : dr.Field<string>("Id"),
                        UserName = dr["UserName"] == DBNull.Value ? "" : dr.Field<string>("UserName"),
                    };
                    usersList.Add(obj);
                }
                return usersList;
            }
            catch (Exception ex) { return null; }
        }


        public ICommand UserValidationCommand
        {
            get
            {
                if (_userValidationCommand == null)
                {
                    _userValidationCommand = new RelayCommand(
                        param => ValidateUser()
                    );
                }
                return _userValidationCommand;
            }
        }
        #endregion

        #region Methods
        public void ValidateUser()
        {
            try
            {
                IPageViewModel pageViewModel = new PatientSelectionViewModel();
                DataTable dtUsers = DataBaseHelper.ExecuteSelectQuery("Select Id from Users Where Id = '" + selectedUserId.Id + "' and Password = '" + PasswordHandler() + "'");
                if (dtUsers.Rows.Count == 1)
                {
                    //GotoPatientView(selectedUserId.Id);
                    ApplicationViewModel._eventAggregator.GetEvent<PubSubEvent<IPageViewModel>>().Publish(pageViewModel);

                }
                else
                {
                    MessageBox.Show("Unable to Login, Verify Details.");
                }
            }
            catch (Exception ex) { }
        }
        #endregion


    }
}