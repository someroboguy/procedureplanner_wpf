﻿using CapstoneDAL;
using CapstoneUI.Model;
using Prism.Events;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Input;

namespace CapstoneUI.ViewModel
{
    public class OperationBuilderViewModel : ObservableObject, IPageViewModel
    {
        #region Properties/Variables
        public string Name => "Operation Builder";

        private ObservableCollection<OperationBuilderModel> _patientDetails;
        private ObservableCollection<OperationBuilderModel> _proceduresDecomList;
        private ObservableCollection<OperationBuilderModel> _proceduresFusionList;
        private ObservableCollection<OperationBuilderModel> _proceduresInstList;
        private ObservableCollection<OperationBuilderModel> _operationProceduresList;
        private ICommand _selectedProcedureCommand;
        private ICommand _deleteOperationProcCommand;
        private ICommand _shiftUpCommand;
        private ICommand _shiftDownCommand;
        public OperationBuilderModel _operationProcId;
        public static OperationBuilderModel _selectedOperationProcId;
        public static EventAggregator _operationEventAggregator;
        string patientId;

        //public OperationBuilderViewModel()
        //{
        //    EventAggregator eventAggregator = new EventAggregator();
        //    _operationEventAggregator = eventAggregator;
        //    _operationEventAggregator.GetEvent<PubSubEvent<string>>().Subscribe((eventArgs) => GetPatientDetail(eventArgs));
        //}

        public OperationBuilderModel SelectedOperationProcId
        {
            get => _operationProcId;
            set
            {
                _operationProcId = value;
                _selectedOperationProcId = _operationProcId;
                OnPropertyChanged("SelectedOperationProcId");
            }
        }
        public ObservableCollection<OperationBuilderModel> PatientDetails
        {
            get
            {
                if (this._patientDetails == null)
                {
                    this._patientDetails = this.GetPatientDetail(patientId);
                }
                return this._patientDetails;
            }
        }
        public ObservableCollection<OperationBuilderModel> ProceduresDecomList
        {
            get
            {
                if (this._proceduresDecomList == null)
                {
                    this._proceduresDecomList = this.GetProceduresList("Decompression");
                }
                return this._proceduresDecomList;
            }
        }
        public ObservableCollection<OperationBuilderModel> ProceduresFusionList
        {
            get
            {
                if (this._proceduresFusionList == null)
                {
                    this._proceduresFusionList = this.GetProceduresList("Fusion");
                }
                return this._proceduresFusionList;
            }
        }
        public ObservableCollection<OperationBuilderModel> ProceduresInstList
        {
            get
            {
                if (this._proceduresInstList == null)
                {
                    this._proceduresInstList = this.GetProceduresList("Instrumentation");
                }
                return this._proceduresInstList;
            }
        }

        #endregion

        #region ICommands

        public ICommand SelectedProcedureCommand
        {
            get
            {
                if (_selectedProcedureCommand == null)
                {
                    _selectedProcedureCommand = new RelayCommand(
                        p => AddToSelectedProceduresList(p));
                }

                return _selectedProcedureCommand;
            }
        }

        public ICommand DeleteOperationProcCommand
        {
            get
            {
                if (_deleteOperationProcCommand == null)
                {
                    _deleteOperationProcCommand = new RelayCommand(
                        p => DeleteOperationsProcedureItem());
                }

                return _deleteOperationProcCommand;
            }
        }

        public ICommand ShiftUpCommand
        {
            get
            {
                if (_shiftUpCommand == null)
                {
                    _shiftUpCommand = new RelayCommand(
                        p => ShiftUp());
                }

                return _shiftUpCommand;
            }
        }

        public ICommand ShiftDownCommand
        {
            get
            {
                if (_shiftDownCommand == null)
                {
                    _shiftDownCommand = new RelayCommand(
                        p => ShiftDown());
                }

                return _shiftDownCommand;
            }
        }

        #endregion

        #region Methods

        public ObservableCollection<OperationBuilderModel> OperationProceduresList
        {
            get
            {
                if (this._operationProceduresList == null)
                {
                    this._operationProceduresList = this.GetOperationProceduresList(PatientDetails[0].Id);
                }
                return this._operationProceduresList;
            }
        }

        public ObservableCollection<OperationBuilderModel> GetPatientDetail(string patientId)
        {
            try
            {
                DataTable dtPatient = DataBaseHelper.ExecuteSelectQuery("Select * from Patients Where Id = '2';");
                ObservableCollection<OperationBuilderModel> patientList = new ObservableCollection<OperationBuilderModel>();
                foreach (DataRow dr in dtPatient.Rows)
                {
                    var obj = new OperationBuilderModel()
                    {
                        Id = dr["Id"] == DBNull.Value ? "" : dr.Field<string>("Id"),
                        PatientName = dr["PatientName"] == DBNull.Value ? "" : dr.Field<string>("PatientName"),
                        BirthDate = dr["BirthDate"] == DBNull.Value ? "" : dr.Field<string>("BirthDate"),
                        BirthPlace = dr["BirthPlace"] == DBNull.Value ? "" : dr.Field<string>("BirthPlace"),
                        BloodType = dr["BloodType"] == DBNull.Value ? "" : dr.Field<string>("BloodType"),
                        Height = dr["Height"] == DBNull.Value ? "" : dr.Field<string>("Height"),
                        Weight = dr["Weight"] == DBNull.Value ? "" : dr.Field<string>("Weight"),
                        Sex = dr["Sex"] == DBNull.Value ? "" : dr.Field<string>("Sex"),
                        Insurance = dr["Insurance"] == DBNull.Value ? "" : dr.Field<string>("Insurance"),
                        Medication = dr["Medication"] == DBNull.Value ? "" : dr.Field<string>("Medication"),
                        Instructions = dr["Instructions"] == DBNull.Value ? "" : dr.Field<string>("Instructions"),
                        Staus = dr["Staus"] == DBNull.Value ? "" : dr.Field<string>("Staus"),
                        Refills = dr["Refills"] == DBNull.Value ? "" : dr.Field<string>("Refills"),
                        LastFilledOn = dr["LastFilledOn"] == DBNull.Value ? "" : dr.Field<string>("LastFilledOn"),
                        InitiallyOrdered = dr["InitiallyOrdered"] == DBNull.Value ? "" : dr.Field<string>("InitiallyOrdered"),
                        Quanity = dr["Quanity"] == DBNull.Value ? "" : dr.Field<string>("Quanity"),
                        DaysSupply = dr["DaysSupply"] == DBNull.Value ? "" : dr.Field<string>("DaysSupply"),
                        Pharmacy = dr["Pharmacy"] == DBNull.Value ? "" : dr.Field<string>("Pharmacy"),
                        PrescriptionNumber = dr["PrescriptionNumber"] == DBNull.Value ? "" : dr.Field<string>("PrescriptionNumber"),
                        AllergyName = dr["AllergyName"] == DBNull.Value ? "" : dr.Field<string>("AllergyName"),
                        DateEntered = dr["DateEntered"] == DBNull.Value ? "" : dr.Field<string>("DateEntered"),
                        Reaction = dr["Reaction"] == DBNull.Value ? "" : dr.Field<string>("Reaction"),
                        AllergyType = dr["AllergyType"] == DBNull.Value ? "" : dr.Field<string>("AllergyType"),
                        DrugClass = dr["DrugClass"] == DBNull.Value ? "" : dr.Field<string>("DrugClass"),
                        ObservedHistorical = dr["ObservedHistorical"] == DBNull.Value ? "" : dr.Field<string>("ObservedHistorical"),
                    };
                    patientList.Add(obj);
                }

                //IPageViewModel pageViewModel = new OperationBuilderViewModel();
                //ApplicationViewModel._eventAggregator.GetEvent<PubSubEvent<IPageViewModel>>().Publish(pageViewModel);
                return patientList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ObservableCollection<OperationBuilderModel> GetProceduresList(string procCatType)
        {
            try
            {
                DataTable dtProcedures = DataBaseHelper.ExecuteSelectQuery("Select Id, ProcedureName, ProcedureCategoryId, ImageSource from Procedures Where ProcedureCategoryId = '" + procCatType + "';");
                ObservableCollection<OperationBuilderModel> proceduresList = new ObservableCollection<OperationBuilderModel>();
                foreach (DataRow dr in dtProcedures.Rows)
                {
                    var obj = new OperationBuilderModel()
                    {
                        Id = dr["Id"] == DBNull.Value ? "" : dr.Field<string>("Id"),
                        ProcedureName = dr["ProcedureName"] == DBNull.Value ? "" : dr.Field<string>("ProcedureName"),
                        ProcedureCategoryName = dr["ProcedureCategoryId"] == DBNull.Value ? "" : dr.Field<string>("ProcedureCategoryId"),
                        ImageSource = dr["ImageSource"] == DBNull.Value ? "" : dr.Field<string>("ImageSource"),
                    };
                    proceduresList.Add(obj);
                }
                return proceduresList;
            }
            catch (Exception ex) { return null; }
        }
      

        public ObservableCollection<OperationBuilderModel> GetOperationProceduresList(object patientId)
        {
            try
            {
                DataTable dtProcedures = DataBaseHelper.ExecuteSelectQuery("Select Id, ProcNameWithNum from OperationProcedureList Where PatientId = '" + patientId + "' and IsDeleted = 0 Order By Id;");
                ObservableCollection<OperationBuilderModel> proceduresList = new ObservableCollection<OperationBuilderModel>();
                foreach (DataRow dr in dtProcedures.Rows)
                {
                    var obj = new OperationBuilderModel()
                    {
                        OperationProcId = dr["Id"] == DBNull.Value ? -1 : dr.Field<int>("Id"),
                        ProcedureName = dr["ProcNameWithNum"] == DBNull.Value ? "" : dr.Field<string>("ProcNameWithNum")
                    };
                    proceduresList.Add(obj);
                }
                return proceduresList;
            }
            catch (Exception ex) { return null; }
        }

        #endregion

        #region Private Methods

        private void DeleteOperationsProcedureItem()
        {
            try
            {
                DataBaseHelper.ExecuteSQLScalar("UPDATE OperationProcedureList SET IsDeleted = 1 WHERE PatientId = '" + PatientDetails[0].Id + "'AND Id = '" + _selectedOperationProcId.OperationProcId + "'");
                //  _selectedProcedureCommand = new RelayCommand(p => GetOperationProceduresList(PatientDetails[0].Id));
            }
            catch (Exception ex) { }
        }

        private void AddToSelectedProceduresList(object procedureName)
        {
            string patientId = PatientDetails[0].Id;
            try
            {
                DataBaseHelper.ExecuteSQLNonQuery("Save_Operation_Procedure_List", patientId, procedureName);
                if (_operationProceduresList != null)
                {
                    _operationProceduresList.ToList().All(i => _operationProceduresList.Remove(i));
                }
                DataTable dtProcedures = DataBaseHelper.ExecuteSelectQuery("Select Id, ProcNameWithNum from OperationProcedureList Where PatientId = '" + patientId + "' and IsDeleted = 0 Order By Id;");
                foreach (DataRow dr in dtProcedures.Rows)
                {
                    var obj = new OperationBuilderModel()
                    {
                        OperationProcId = dr["Id"] == DBNull.Value ? -1 : dr.Field<int>("Id"),
                        ProcedureName = dr["ProcNameWithNum"] == DBNull.Value ? "" : dr.Field<string>("ProcNameWithNum")
                    };
                    _operationProceduresList.Add(obj);
                }
            }
            catch (Exception ex) { }
        }

        public void ShiftUp()
        {
            DataBaseHelper.ExecuteSQLNonQuery("ShiftUp_Operation_Procedure_List", PatientDetails[0].Id, _selectedOperationProcId.ProcedureName);
        }

        public void ShiftDown()
        {
            DataBaseHelper.ExecuteSQLNonQuery("ShiftDown_Operation_Procedure_List", PatientDetails[0].Id, _selectedOperationProcId.ProcedureName);
        }

        #endregion
    }
}
