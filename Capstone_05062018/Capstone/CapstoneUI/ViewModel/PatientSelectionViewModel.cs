﻿using CapstoneDAL;
using CapstoneUI.Model;
using Prism.Events;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Input;

namespace CapstoneUI.ViewModel
{
    public class PatientSelectionViewModel : ObservableObject, IPageViewModel
    {
        public string Name => "Patient Selection";
        public string _userId;
        public PatientSelectionModel _selectedPatientCommand;
        public static PatientSelectionModel selectedPatientId;
        private ICommand _patientCommand;


        public string UserId
        {
            get => _userId;
            set
            {
                _userId = value;
                OnPropertyChanged("UserId");
            }
        }
        private ObservableCollection<PatientSelectionModel> patients;

        public ObservableCollection<PatientSelectionModel> Patients
        {
            get
            {
                if (this.patients == null)
                {
                    this.patients = this.GetPatients();
                }
                return this.patients;
            }
        }

        public ObservableCollection<PatientSelectionModel> GetPatients()
        {
            try
            {
                DataTable dtPatients = DataBaseHelper.ExecuteSelectQuery("Select Id,PatientName from Patients");
                ObservableCollection<PatientSelectionModel> patientList = new ObservableCollection<PatientSelectionModel>();
                foreach (DataRow dr in dtPatients.Rows)
                {
                    var obj = new PatientSelectionModel()
                    {
                        Id = dr["Id"] == DBNull.Value ? "" : dr.Field<string>("Id"),
                        PatientName = dr["PatientName"] == DBNull.Value ? "" : dr.Field<string>("PatientName"),
                    };
                    patientList.Add(obj);
                }
                return patientList;
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public PatientSelectionModel SelectedPatient
        {
            get => _selectedPatientCommand;
            set
            {
                _selectedPatientCommand = value;
                selectedPatientId = _selectedPatientCommand;
            }
        }

        public ICommand PatientCommand
        {
            get
            {
                if (_patientCommand == null)
                {
                    _patientCommand = new RelayCommand(
                        p => LoadOperationBuilderView(p));
                }
                return _patientCommand;
            }
        }



    public void LoadOperationBuilderView(object p)
        {
            try

            {
                //string patientId = selectedPatientId.Id;
                //OperationBuilderViewModel._operationEventAggregator.GetEvent<PubSubEvent<string>>().Publish(patientId);
                IPageViewModel pageViewModel = new OperationBuilderViewModel();
                ApplicationViewModel._eventAggregator.GetEvent<PubSubEvent<IPageViewModel>>().Publish(pageViewModel);
            }
            catch (Exception ex) { }
        }

     


    }
}
