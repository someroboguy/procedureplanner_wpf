﻿using CapstoneUI.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace CapstoneUI.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
            DataContext = new LoginViewModel();
            LoginViewModel.PasswordHandler = () => passwordBox.Password;
        }


        #region buttonValues
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "1";
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "2";
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "3";
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "4";
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "5";
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "6";
        }

        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "7";
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "8";
        }

        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = passwordBox.Password + "9";
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            passwordBox.Password = null;
        }
        #endregion
    }
}
